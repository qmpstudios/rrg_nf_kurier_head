/* 
 * File:   example_robot.cpp
 * Author: mateusz - RRG@WUT rrg.mchtr.pw.edu.pl
 * Author: pweclewski - RRG@WUT rrg.mchtr.pw.edu.pl
 * 
 * This is a example_robot class - an example for nfv2 communication
 * Please modify this example_robot class for your hardware
 * 
 * Created on December 6, 2012, 7:39 PM
 */

#include "rrg_nf/example_robot.h"

example_robot::example_robot(ros::NodeHandle nh, tf::TransformListener& tf) :
n(nh), _tf(tf), rxCnt(0), txCnt(0)
{
    //read parameters if defined in launch files...
    ros::NodeHandle n_private("~");
    //n_private.param<bool> ("map_compare", map_compare, true);

    NFv2_Config2(&NFComBuf, NF_PCAddress, NF_MainModuleAddress);
    crcInit();


    portName = "/dev/ttyACM0";
    //portName = "/dev/ttyUSB0";

    CommPort = new SerialComm();
    //    NFComBuf.SetDrivesSpeed.data[0] = 0;
    //    NFComBuf.SetDrivesSpeed.data[1] = 0;

    CommPort->connect(portName, 57600);

    if (!(CommPort->isOpen()))
    {
        std::cerr << "Connection failed to " << portName << std::endl;
    }
    else
    {
        std::cout << "Connected to " << portName << std::endl;
    }

    NFComBuf.SetDrivesMode.data[0] = NF_DrivesMode_POSITION;
    commandArray[commandCnt++] = NF_COMMAND_SetDrivesMode;

    robot = new Robot(); // creates specific robot object
    stupidcounter = 0;

    subCmdVel = n.subscribe("cmd_vel", 1, &example_robot::callbackCmdVel, this);
    subFHeadDrives = n.subscribe("fheaddrives", 1, &example_robot::callbackFHeadDrives, this);

}

example_robot::~example_robot()
{
    setMotorsSpeeds(0, 0);
}

void example_robot::readFromRobot()
{

    int global_counter = 0;
    int n = 0;
    while ((n = CommPort->serialRead(&rxBuf[this->rxCnt], 1)) == 1)
    {
        global_counter++; // temporary timeout counter.. (better to do this in time.now)
        if (global_counter > 20000)
        {
            std::cerr << "\n!!! timeout " << global_counter << std::endl;
            return;
        }
        //        std::cout << "after read ";
        //
        //        std::cout << "\n Attempt with: " << n << " result \t";

        if (n < 0)
        {
            perror("read");
            exit(EXIT_FAILURE);
        }
        else if (n > 0)
        {


            //            for (int i = 0; i < this->rxCnt; i++)
            //            {
            //                std::cout << std::hex << (uint) rxBuf[i] << " ";
            //            }
            //            std::cout << std::dec << "\n";
        }

        // std::cout << "\n no:" << global_counter << " Bytes IN: " << n << " \t";

        uint8_t commArr[16], commCnt, bytesReceived;

        if ((bytesReceived = NF_Interpreter(&NFComBuf, rxBuf, &(this->rxCnt), commArr, &commCnt)) > 0)
        {
            std::cout << std::dec << "NF_Interpreter received " << (int) bytesReceived << " bytes\n";
            std::cout << std::dec << "Frame from robot: ";

            break;
            //sleep(5);
        }
        if (this->rxCnt == 255)
        {
            this->rxCnt = 0;
            break;
        }

    }
}

void example_robot::sendToRobot()
{
    // If communication with requested
    if (commandCnt > 0)
    {
        std::cout << "************************* 1 cycle **********************************************\n";
        txCnt = NF_MakeCommandFrame(&NFComBuf, txBuf, (const uint8_t*) commandArray, commandCnt, NF_MainModuleAddress);
        // Clear communication request
        commandCnt = 0;
        std::cout << "Frame to robot (" << (uint16_t) txCnt << " bytes): ";
        for (int i = 0; i < txCnt; i++)
        {
            std::cout << std::hex << (uint) txBuf[i] << " ";
        }
        std::cout << std::dec << "\n";

        CommPort->serialWrite(txBuf, txCnt);

    }

}

// Robot high level functions -------------------------------------------------

void example_robot::robotUpdate() // Synchronism of communication
{

    //stupidcounter = stupidcounter + 0.01;
    // setMotorsSpeeds(robot->robot_speed_left, robot->robot_speed_right);
    // robotSetDO();
    //int temp = 1050 + 850 * abs(sin(stupidcounter));
    //setRobotServos();
    //setMotorsPosition();
    robotGetAI();

    std::cout << "AI:\t";
    for (int i = 0; i < NF_BUFSZ_ReadAnalogInputs; i++)
        std::cout << std::dec << NFComBuf.ReadAnalogInputs.data[i] << " ";
    std::cout << "\n";
}

void example_robot::robotInit() // Initalization of robot
{
    setMotorsSpeeds(0, 0);
    robot->robot_speed_left = 0;
    robot->robot_speed_right = 0;
}

void example_robot::robotPanic() // Emergency stop function
{
    setMotorsSpeeds(0, 0);
}

// CALLBACKs  from ROS ------------------------------------------------------

void example_robot::callbackCmdVel(const geometry_msgs::TwistConstPtr &msg)
{
    //  //moje
    //  ROS_DEBUG("new speed: [%0.2f,%0.2f]", msg->linear.x, msg->angular.z);
    //
    //  vel[0] = msg->linear.x;
    //  vel[1] = msg->angular.z;
    //
    //  cmdvel_ = *msg;
    //
    //
    //  m_debug_odom.twist.twist.linear.x = vel[0];
    //  m_debug_odom.twist.twist.angular.z = vel[1];
    //
    //  m_vel_updated = true;

    robot->robot_speed_left = 10 * msg->linear.x + 20 * msg->angular.z;
    robot->robot_speed_right = 10 * msg->linear.x - 20 * msg->angular.z;
}

void example_robot::callbackFHeadDrives(const rrg_nf_kurier_head::fheaddrives &msg2)
{
    setRobotServos(msg2);
    setMotorsPositions(msg2);
}

// Robot specific functions ---------------------------------------------------

void example_robot::robotGetVitals()
{

    commandArray[commandCnt++] = NF_COMMAND_ReadDeviceVitals;

    sendToRobot();
    readFromRobot();
}

void example_robot::robotSetDO(int8_t SetDO)
{
    commandArray[commandCnt++] = NF_COMMAND_SetDigitalOutputs;
    NFComBuf.SetDigitalOutputs.data[0] = SetDO;
    sendToRobot();
}

void example_robot::robotSetAO()
{
    //    commandArray[commandCnt++] = NF_COMMAND_SetAnalog....;
    //    ...
    //    sendToRobot();
}

void example_robot::robotGetDI()
{
    commandArray[commandCnt++] = NF_COMMAND_ReadDigitalInputs;

    sendToRobot();
    readFromRobot();
}

void example_robot::robotGetAI()
{

    commandArray[commandCnt++] = NF_COMMAND_ReadAnalogInputs;

    sendToRobot();
    readFromRobot();
}

void example_robot::setRobotServos(const rrg_nf_kurier_head::fheaddrives &msg2)
{
    
    commandArray[commandCnt++] = NF_COMMAND_SetServosPosition;
    NFComBuf.SetServosPosition.data[0] = msg2.left_eyebrow;
    NFComBuf.SetServosPosition.data[1] = msg2.right_eyebrow;
    NFComBuf.SetServosPosition.data[2] = msg2.left_eyelid;
    NFComBuf.SetServosPosition.data[3] = msg2.right_eyelid;
    NFComBuf.SetServosPosition.data[4] = msg2.left_eyeball;
    NFComBuf.SetServosPosition.data[5] = msg2.right_eyeball;
    NFComBuf.SetServosPosition.data[6] = msg2.eyeline;
    NFComBuf.SetServosPosition.data[7] = msg2.mouth;
    NFComBuf.SetServosPosition.data[8] = 1500;
    NFComBuf.SetServosPosition.data[9] = 1500;
    NFComBuf.SetServosPosition.data[10] = 1500;

    sendToRobot();
}

void example_robot::setMotorsPositions(const rrg_nf_kurier_head::fheaddrives &msg2)
{
    commandArray[commandCnt++] = NF_COMMAND_SetDrivesPosition;
    commandArray[commandCnt++] = NF_COMMAND_SetDrivesMode;
    
    NFComBuf.SetDrivesMode.data[0] = NF_DrivesMode_POSITION;
    NFComBuf.SetDrivesMode.data[1] = NF_DrivesMode_POSITION;
    NFComBuf.SetDrivesMode.data[2] = NF_DrivesMode_POSITION;
    NFComBuf.SetDrivesMode.data[3] = NF_DrivesMode_POSITION;
    
    NFComBuf.SetDrivesPosition.data[0] = msg2.nod; //nod
    NFComBuf.SetDrivesPosition.data[1] = msg2.tilt; //tilt
    NFComBuf.SetDrivesPosition.data[2] = msg2.turn; //turn
    NFComBuf.SetDrivesPosition.data[3] = 1500;
    
    sendToRobot();
 
}

void example_robot::setMotorsSpeeds(int motor_left, int motor_right)
{

    if (motor_left > robot_left_max)
    {
        motor_left = robot_left_max;
        std::cout << "speed exceeded maximum...\n";
    }
    if (motor_right > robot_right_max)
    {
        motor_right = robot_right_max;
        std::cout << "speed exceeded maximum...\n";
    }

    NFComBuf.SetDrivesMode.data[0] = NF_DrivesMode_SPEED;
    NFComBuf.SetDrivesMode.data[1] = NF_DrivesMode_SPEED;
    commandArray[commandCnt++] = NF_COMMAND_SetDrivesMode;

    //    NFComBuf.SetDrivesSpeed.data[0] = motor_left;
    //    NFComBuf.SetDrivesSpeed.data[1] = motor_right;
    commandArray[commandCnt++] = NF_COMMAND_SetDrivesSpeed;

    sendToRobot();

}
