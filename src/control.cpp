/* 
 * File:   control.cpp
 * Author: pawel
 *
 * Created on 14 grudzień 2012, 14:41
 */

#include "rrg_nf/emotions.h"

using namespace std;
using namespace ros;

/*
 * 
 */
int main(int argc, char** argv) 
{
    ros::init(argc, argv, "control");
    ros::NodeHandle n;
    ros::Time::init();
    ros::Rate loop_rate(25);
    
    int c, b = 0;
        
    emotions *set = new emotions(n);
    
    cout << endl << "Choose mode" << endl << endl;
    cout << "For users type 0" << endl;
    cout << "For demo type 1" << endl;
    cout << "For face tracking type 2" << endl;
    cout << "For advanced tracking type 3" << endl;

    cout << "Mode choice:  ";
    cin >> c;
//    c = 3;
    cout << endl;
    
    set->head_zero();
    
    while (ros::ok())
    {    
        switch (c)
        {
            case 0:
                set->choice();
            break;

            case 1:
            {                
                while (ros::ok())
                { 
                    set->demo(b);
                    loop_rate.sleep();
                }
            }
            break;
            
            case 2:
            {
                set->head_zero();
                
                while (ros::ok())
                {
                    ros::spinOnce();
                    set->Camera_Follower();
                    loop_rate.sleep();
                }
            }
            break;
            
            case 3:
            {
//                headfollower *set2 = new headfollower(n);
//                set2->head_zero();
                
                while (ros::ok())
                {
                    ros::spinOnce();
                    set->Advanced_Follower();
                    loop_rate.sleep();
                }
            }
            break;

            default:
            break;
        }
    
        loop_rate.sleep();
    }
    
    return 0;
}