/* 
 * File:   nf_driver.cpp
 * Author: mateusz - RRG@WUT rrg.mchtr.pw.edu.pl
 * 
 * This is a classic ROS node using specific example_robot class.
 * Please modify example_robot for your hardware
 * 
 * Created on December 6, 2012, 7:39 PM
 */


#include <iostream>
#include <time.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <rosgraph_msgs/Clock.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include "rrg_nf/example_robot.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "nf_driver");
    ros::NodeHandle n;
    ros::Time::init();
    ros::Rate loop_rate(25);
    tf::TransformListener tf;

    example_robot *robot = new example_robot(n, tf);

   // robot->robotInit();

    while (ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
        robot->robotUpdate();
    }

    return 0;
}
