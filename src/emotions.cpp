#include "rrg_nf/emotions.h"

using namespace std;
using namespace ros;

emotions::emotions(NodeHandle n)
{    
    pubfheaddrives = n.advertise<rrg_nf_kurier_head::fheaddrives> ("fheaddrives", 1);
    subPointToFollow = n.subscribe("/point_to_follow", 1, &emotions::callbackPointToFollow, this);
    subPeople = n.subscribe("/people", 1, &emotions::callbackPeople, this);
}

emotions::~emotions()
{
    
}

void emotions::callbackPointToFollow(const kurier_face_tracker::pointtofollow &facecenter)
{
    facecenter_global = facecenter;
}

void emotions::callbackPeople(const rrg_navigation_dynamic_msgs::people &people)
{    
    people_global = people;
}

rrg_nf_kurier_head::fheaddrives emotions::check_range(rrg_nf_kurier_head::fheaddrives fdrives1)
{
    if (fdrives1.left_eyebrow < left_eyebrow_min)
        fdrives1.left_eyebrow = left_eyebrow_min;
    else if (fdrives1.left_eyebrow > left_eyebrow_max)
        fdrives1.left_eyebrow = left_eyebrow_max;
    
    if (fdrives1.right_eyebrow < right_eyebrow_min)   
        fdrives1.right_eyebrow = right_eyebrow_min;
    else if (fdrives1.right_eyebrow > right_eyebrow_max)
        fdrives1.right_eyebrow = right_eyebrow_max;
        
    if (fdrives1.left_eyelid < left_eyelid_min)
        fdrives1.left_eyelid = left_eyelid_min;
    else if (fdrives1.left_eyelid > left_eyelid_max)
        fdrives1.left_eyelid = left_eyelid_max;
    
    if (fdrives1.right_eyelid < right_eyelid_min)
        fdrives1.right_eyelid = right_eyelid_min;
    else if (fdrives1.right_eyelid > right_eyelid_max)
        fdrives1.right_eyelid = right_eyelid_max;
      
    if (fdrives1.left_eyeball < left_eyeball_min)
        fdrives1.left_eyeball = left_eyeball_min;
    else if (fdrives1.left_eyeball > left_eyeball_max)
        fdrives1.left_eyeball = left_eyeball_max;
    
    if (fdrives1.right_eyeball < right_eyeball_min)
        fdrives1.right_eyeball = right_eyeball_min;
    else if (fdrives1.right_eyeball > right_eyeball_max)
        fdrives1.right_eyeball = right_eyeball_max;
    
    if (fdrives1.eyeline < eyeline_min)
        fdrives1.eyeline = eyeline_min;
    else if (fdrives1.eyeline > eyeline_max)
        fdrives1.eyeline = eyeline_max;
    
    if (fdrives1.mouth < mouth_min)
        fdrives1.mouth = mouth_min;
    else if (fdrives1.mouth > mouth_min)
        fdrives1.mouth = mouth_max;
    
    if (fdrives1.nod < nod_min)
        fdrives1.nod = nod_min;
    else if (fdrives1.nod > nod_max)
        fdrives1.nod = nod_max;
    
    if (fdrives1.tilt < tilt_min)   
        fdrives1.tilt = tilt_min;
    else if (fdrives1.tilt > tilt_max)
        fdrives1.tilt = tilt_max;
        
    if (fdrives1.turn < turn_min)
        fdrives1.turn = turn_min;
    else if (fdrives1.turn > turn_max)
        fdrives1.turn = turn_max;
    
    return fdrives1;
}

void emotions::users_faces()
{
    int s0, s1, s2, s3, s4, s5, s6, s7;
    
    cout << "Value of left_eyebrow <0-700>: ";
    cin >> s0;
    cout << endl;
    fdrives2.left_eyebrow = left_eyebrow_min + s0;

    cout << "Value of right_eyebrow <0-700>: ";
    cin >> s1;
    cout << endl;
    fdrives2.right_eyebrow = right_eyebrow_max - s1;

    cout << "Value of left_eyelid <0-630>: ";
    cin >> s2;
    cout << endl;
    fdrives2.left_eyelid = left_eyelid_max - s2;
    
    cout << "Value of right_eyelid <0-600>: ";
    cin >> s3;
    cout << endl;
    fdrives2.right_eyelid = right_eyelid_min + s3;

    cout << "Value of left_eyeball <0-600>: ";
    cin >> s4;
    cout << endl;
    fdrives2.left_eyeball = left_eyeball_max - s4;

    cout << "Value of right_eyeball <0-560>: ";
    cin >> s5;
    cout << endl;
    fdrives2.right_eyeball = right_eyeball_min + s5;

    cout << "Value of eyeline <0-450>: ";
    cin >> s6;
    cout << endl;
    fdrives2.eyeline = eyeline_min + s6;

    cout << "Value of mouth <0-450>: ";
    cin >> s7;
    cout << endl;
    fdrives2.mouth = mouth_min + s7;

    fdrives1 = motors_zero(fdrives1);
    fdrives1 = check_range(fdrives1);
    
    pubfheaddrives.publish(fdrives1);
}

void emotions::happiness()
{
    fdrives1.left_eyebrow = 1150;
    fdrives1.right_eyebrow = 1850;
    fdrives1.left_eyelid = 1200;
    fdrives1.right_eyelid = 1750;
    fdrives1.left_eyeball = 1550;
    fdrives1.right_eyeball = 1520;
    fdrives1.eyeline = 1545;
    fdrives1.mouth = 1050;
    fdrives1 = motors_zero(fdrives1);
    
    pubfheaddrives.publish(fdrives1);
}

void emotions::anger()
{
    fdrives1.left_eyebrow = 1850;
    fdrives1.right_eyebrow = 1150;
    fdrives1.left_eyelid = 1515;
    fdrives1.right_eyelid = 1450;
    fdrives1.left_eyeball = 1550;
    fdrives1.right_eyeball = 1520;
    fdrives1.eyeline = 1545;
    fdrives1.mouth = 1275;
    fdrives1.nod = (nod_min + nod_max)/2;
    fdrives1.tilt = (tilt_min + tilt_max)/2;
    fdrives1.turn = (turn_min + turn_max)/2;
    
    pubfheaddrives.publish(fdrives1);
}

void emotions::move_head()
{
    int m0, m1, m2;
    
    cout << "Value of nod <0-250>: ";
    cin >> m0;
    cout << endl;
    fdrives1.nod = nod_min + m0;
    
    cout << "Value of tilt <0-250>: ";
    cin >> m1;
    cout << endl;
    fdrives1.tilt = tilt_min + m1;
    
    cout << "Value of turn <0-2200>: ";
    cin >> m2;
    cout << endl;
    fdrives1.turn = turn_min + m2;
    
    fdrives1 = servos_zero(fdrives1);    
    fdrives1 = check_range(fdrives1);
    
    pubfheaddrives.publish(fdrives1);
}

rrg_nf_kurier_head::fheaddrives emotions::servos_zero(rrg_nf_kurier_head::fheaddrives fdrives1)
{
    fdrives1.left_eyebrow = (left_eyebrow_min + left_eyebrow_max)/2;
    fdrives1.right_eyebrow = (right_eyebrow_min + right_eyebrow_max)/2;
    fdrives1.left_eyelid = left_eyelid_min;//(left_eyelid_min + left_eyelid_max)/2;
    fdrives1.right_eyelid = right_eyelid_max;//(right_eyelid_min + right_eyelid_max)/2;
    fdrives1.left_eyeball = (left_eyeball_min + left_eyeball_max)/2;
    fdrives1.right_eyeball = (right_eyeball_min + right_eyeball_max)/2;
    fdrives1.eyeline = (eyeline_min + eyeline_max)/2;
    fdrives1.mouth = (mouth_min + mouth_max)/2; 
    return fdrives1;
}

rrg_nf_kurier_head::fheaddrives emotions::motors_zero(rrg_nf_kurier_head::fheaddrives fdrives1)
{
    fdrives1.nod = (nod_min + nod_max)/2;
    fdrives1.tilt = (tilt_min + tilt_max)/2;
    fdrives1.turn = (turn_min + turn_max)/2;    
    return fdrives1;
}

void emotions::yes_nod()
{
    int a = 1;
    int d = 0;
    
    fdrives1 = servos_zero(fdrives1);
    fdrives1 = motors_zero(fdrives1);
    fdrives1.turn = (turn_min + turn_max)/2;
    fdrives1.tilt = (tilt_min + tilt_max)/2;

    while(d < 2)
    {   
        if (d % 2 == 0)
        {
            fdrives1.nod = nod_min;  
            
            pubfheaddrives.publish(fdrives1);

            d++;

            sleep(a);
        }
        
        if (d % 2 == 1)
        {
            fdrives1.nod = nod_max;   

            pubfheaddrives.publish(fdrives1);

            d++;

            sleep(a);
        }
    }
    
    fdrives1 = servos_zero(fdrives1);
    fdrives1 = motors_zero(fdrives1);
    
    pubfheaddrives.publish(fdrives1);
}

void emotions::no_turn()
{
    int a = 1;
    int d = 0;
    
    fdrives1 = servos_zero(fdrives1);
    fdrives1 = motors_zero(fdrives1);
    fdrives1.nod = (nod_min + nod_max)/2;
    fdrives1.tilt = (tilt_min + tilt_max)/2;

    while(d < 2)
    {   
        if (d % 2 == 0)
        {
            fdrives1.turn = turn_min + quarter_turn;  

            pubfheaddrives.publish(fdrives1);

            d++;

            sleep(a);
        }
        
        if (d % 2 == 1)
        {
            fdrives1.turn = turn_max - quarter_turn;   

            pubfheaddrives.publish(fdrives1);

            d++;

            sleep(a);
        }
    }
    
    fdrives1 = servos_zero(fdrives1);
    fdrives1 = motors_zero(fdrives1);
    
    pubfheaddrives.publish(fdrives1);
}

void emotions::left_turn()
{    
    fdrives1.nod = (nod_min + nod_max)/2;
    fdrives1.tilt = (tilt_min + tilt_max)/2;
    fdrives1.turn = turn_max;  

    pubfheaddrives.publish(fdrives1);
}

void emotions::right_turn()
{    
    fdrives1.nod = (nod_min + nod_max)/2;
    fdrives1.tilt = (tilt_min + tilt_max)/2;
    fdrives1.turn = turn_min;  

    pubfheaddrives.publish(fdrives1);
}

void emotions::head_zero()
{    
    fdrives1 = servos_zero(fdrives1);
    fdrives1 = motors_zero(fdrives1);
    
    pubfheaddrives.publish(fdrives1);
}

int emotions::demo(int b)
{    
    if (b % 4 == 0)
    {
        happiness();

//        fdrives1.nod = nod_max;
//        fdrives1.tilt = tilt_max;
//        fdrives1.turn = turn_max - quarter_turn;
        
//        pubfheaddrives.publish(fdrives1);
        
        b++;  
        
        sleep(2);
//        sleep(5);
    }

    cout << "Work checking counter: " << b << endl;
    
    if (b % 4 == 1)
    {
        anger();

//        fdrives1.nod = nod_min;
//        fdrives1.tilt = tilt_min;
//        fdrives1.turn = turn_min + quarter_turn;
//        pubfheaddrives.publish(fdrives1);        
        b++;
//        sleep(5);
        sleep(2);
    }
        
    cout << "Work checking counter: " << b << endl;
    
    if (b % 4 == 2)
    {
        head_zero(); 
        sleep(1);
        yes_nod();              
        b++;
        sleep(3);
    }
        
    cout << "Work checking counter: " << b << endl;
    
    if (b % 4 == 3)
    {
        head_zero();     
        no_turn(); 
        b++;
        sleep(3);
    }
        
    cout << "Work checking counter: " << b << endl;
    
    return b;    
}

void emotions::choice()
{
    int a;
    
    cout << endl << "Choose emotion" << endl << endl;
    cout << "0 for all zero position" << endl;
    cout << "1 for face zero position" << endl;
    cout << "2 for head zero position" << endl;
    cout << "3 for users servos data" << endl;
    cout << "4 for users motors data" << endl;
    cout << "5 for happiness face" << endl;
    cout << "6 for anger face" << endl;
    cout << "7 for yes head nod" << endl;
    cout << "8 for no head turn" << endl;
    cout << "9 for max left turn" << endl;
    cout << "10 for max right turn" << endl;
    
    cout << "Choice:  ";
    cin >> a;
    cout << endl;
    
    switch (a)
    {
        case 0:
        {
            head_zero();     
        }
        break;
    
        case 1:
        {
           fdrives1 = servos_zero(fdrives1);      
           pubfheaddrives.publish(fdrives1);       
        }
        break;
    
        case 2:
        {
           fdrives1 = motors_zero(fdrives1); 
           pubfheaddrives.publish(fdrives1); 
        }
        break;
        
        case 3:
           users_faces();
        break;
    
        case 4:
           move_head(); 
        break;
        
        case 5:
           happiness();
        break;
        
        case 6:
           anger(); 
        break;
        
        case 7:
           yes_nod(); 
        break;
        
        case 8:
           no_turn(); 
        break;
        
        case 9:
           left_turn();
        break;
        
        case 10:
           right_turn();
        break;
        
        default:
                
        break;
    }
}

void emotions::face_follow_move(const kurier_face_tracker::pointtofollow &facecenter)
{    
    // temporary variables of width and height of frame
    temp0 = facecenter.framewidth;
    temp1 = facecenter.frameheight;

//    cout << "temps (" << temp0 << " " << temp1 << ")" << endl; 

    // proportional factors of reinforcement
    double KEyeballs = (left_eyeball_max - left_eyeball_min);
    double KEyeline = (eyeline_max - eyeline_min);
    KEyeballs = KEyeballs / temp0;
    KEyeline = KEyeline / temp1;

//    cout << "K (" << KEyeballs << " " << KEyeline << ")" << endl;    

    // centers of face and captured image
    fcenter.x = facecenter.fpoint_x;
    fcenter.y = facecenter.fpoint_y;
    ccenter.x = facecenter.framewidth / 2;
    ccenter.y = facecenter.frameheight / 2;

//    cout << "face center (" << fcenter.x << " " << fcenter.y << ")" << endl;

    // difference between centers
    xd = fcenter.x - ccenter.x;
    yd = fcenter.y - ccenter.y;

//    cout << "difference (" << xd << " " << yd << ")" << endl;

    // P regulators
    s0 = (ccenter.x + xd) * KEyeballs; // for eyeballs
    s1 = (ccenter.y + yd) * KEyeline; // for eyeline
    
//    cout << "s (" << s0 << " " << s1 << ")" << endl;
    
    // if vector is empty then reset sums
    if (list.size() == 0)
    {
        sum.x = 0;
        sum.y = 0;
    }
    
    // add new element to vector
    difference.x = s0;
    difference.y = s1;
    list.push_back(difference);

    // increase sums
    sum.x = sum.x + s0;
    sum.y = sum.y + s1;
    
//    cout << "sums (" << sum.x << " " << sum.y << ")" << endl;
    
    // if list size is equal to max size
    if (list.size() >= vector_max_size)
    {
        // decrease sums
        sum.x = sum.x - list[0].x;
        sum.y = sum.y - list[0].y;
        
        // erase first element
        list.erase(list.begin());
    }
    
    // count average
    average.x = sum.x / list.size();
    average.y = sum.y / list.size();
    
//    cout << "average (" << average.x << " " << average.y << ")" << endl;
//    cout << "size (" << list.size() << ")" << endl;
    
    // publishing eyes servos values
    fdrives1.left_eyeball = left_eyeball_min + average.x;
    fdrives1.right_eyeball = right_eyeball_min + average.x;
    fdrives1.eyeline = eyeline_max - average.y;

//    cout << "msgs (" << fdrives1.left_eyeball << " " << fdrives1.right_eyeball << " " << fdrives1.eyeline << ")" << endl;

    fdrives1 = check_range(fdrives1);

    pubfheaddrives.publish(fdrives1);
    
    if ((fdrives1.left_eyeball < (left_eyeball_min + close_to_frame_border)) || (fdrives1.left_eyeball > (left_eyeball_max - close_to_frame_border)) || (fdrives1.eyeline < (eyeline_min + close_to_frame_border)) || (fdrives1.eyeline > (eyeline_max - close_to_frame_border)))
    {
        head_follow_move(facecenter, temp0, temp1, average.x, average.y);        
    }
}

void emotions::head_follow_move(const kurier_face_tracker::pointtofollow &facecenter, int framewidth, int frameheight, int avgturn, int avgnod)
{           
    // proportional factors of reinforcement
    double KHeadturn = (turn_max - turn_min);
    double KHeadnod = (nod_max - nod_min);
    KHeadturn = KHeadturn / (framewidth);
    KHeadnod = KHeadnod / frameheight;
    
//    cout << "K (" << KHeadturn << " " << KHeadnod << ")" << endl;  
    
    avgturn = avgturn * KHeadturn;
    avgnod = avgnod *KHeadnod;
    
//    cout << "head_avgs (" << avgturn << " " << avgnod << ")" << endl;  
    
    // publishing head motors values
    fdrives1.turn = turn_max - avgturn;
    fdrives1.nod = nod_max - avgnod;

//    cout << "servos_msgs (" << fdrives1.left_eyeball << " " << fdrives1.right_eyeball << " " << fdrives1.eyeline << ")" << endl;

    fdrives1 = check_range(fdrives1);

    pubfheaddrives.publish(fdrives1);

//    cout << "motors_msgs (" << fdrives1.turn << " " << fdrives1.nod << ")" << endl;
    
    counter++;
//    cout << counter << endl;  
}

void emotions::VectorUpdate(const rrg_navigation_dynamic_msgs::people &people)
{             
    if (people.people.size() == 0)
    {
        templistsize = 0;
        peoplelist.clear();
    }
        
    if (people.people.size() != templistsize)
    {       
        templistsize = 0;
        peoplelist.clear();
        
        for (uint k = 0; k < people.people.size(); k++) //uint because of WARNING from compiler
        {
            tempperson = people.people[k];
            peoplelist.push_back(tempperson);
            templistsize++;
        }
    }
    else
    {
        tempperson = checkDistance(peoplelist);
    }
    
    cout << people_global.people.size() << endl;
}

rrg_navigation_dynamic_msgs::person emotions::checkDistance(vector<rrg_navigation_dynamic_msgs::person> peoplelist)
{        
    for (uint k = 0; k < people.people.size(); k++) //uint because of WARNING from compiler
    {
        if (k == 0)
        {
            tempperson = people.people[k];
        }
        else
        {
            if (tempperson.pose.position.y > peoplelist[k].pose.position.y)
            {
                tempperson = people.people[k];
            }
        }
    }
    
//    cout << "CHECKOUT " << tempperson.id << endl;
    
    return tempperson;
}

void emotions::fromKinectToHead(rrg_navigation_dynamic_msgs::person tempperson)
{               
    // proportional factor of reinforcement
    double KinectRange = (kinect_max - kinect_min);
    double MotorsRange = (turn_max - turn_min);
    KKinect = MotorsRange / KinectRange;

//    cout << "K (" << KKinect << ")" << endl;    

    // center of person from kinect
    kinectvalue = tempperson.pose.position.x;
    
//    cout << "kinect center (" << kinectvalue << ")" << endl;

    // P regulator
    s2 = kinectvalue * KKinect; // for kinect
    
//    cout << "s (" << s2 << ")" << endl;
    //set head values
    fdrives1.turn = turn_min + (MotorsRange / 2) + s2;        
    
//    cout << "msg (" << fdrives1.turn << ")" << endl;

    // publishing eyes servos values
    fdrives1 = check_range(fdrives1);
    pubfheaddrives.publish(fdrives1);       
    
    sleep(2);
}

void emotions::Camera_Follower()
{
    face_follow_move(facecenter_global);
}

void emotions::Advanced_Follower()
{
    tempperson1 = tempperson;
    
    //data update - return info about closest person
    VectorUpdate(people_global);
    
    //find person using kinect
    if((templistsize == 0) || (tempperson1.id != tempperson.id))
    {        
        cout << "LOOKING FOR PERSON" << endl;
        VectorUpdate(people_global);
        
        
//        cout << people.people.size() << endl;
        
        //if found - send data to head
        if (templistsize != 0)
        {
            fromKinectToHead(tempperson);
            cout << "KINECT FOLLOWER" << endl;
        }
    }
    
    //while there is the same person in front of kinect - follow with camera
    else if ((templistsize != 0) && (tempperson1.id == tempperson.id))
    {
        Camera_Follower();     
        cout << "CAMERA FOLLOWER" << endl;
    }
    
    //exception
    else
    {
        cout << "FAIL" << endl;        
    }
}