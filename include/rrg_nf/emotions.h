/* 
 * File:   emotions.h
 * Author: pawel
 *
 * Created on 14 grudzień 2012, 14:49
 */

#ifndef EMOTIONS_H
#define	EMOTIONS_H

#define left_eyebrow_max 1850
#define left_eyebrow_min 1150

#define right_eyebrow_max 1850
#define right_eyebrow_min 1150

#define left_eyelid_max 1830
#define left_eyelid_min 1200

#define right_eyelid_max 1750
#define right_eyelid_min 1150

#define left_eyeball_max 1850
#define left_eyeball_min 1250

#define right_eyeball_max 1800
#define right_eyeball_min 1250

#define eyeline_max 1770
#define eyeline_min 1320

#define mouth_max 1500
#define mouth_min 1050

#define nod_max 1300
#define nod_min 1050

#define tilt_max 1300 // to left side
#define tilt_min 1050 // to right side

#define turn_max 3200 // left
#define turn_min 1000 // right
#define quarter_turn 550

#define vector_max_size 20
#define close_to_frame_border 200

#define kinect_max 1 // to right side
#define kinect_min -1 // to left side

#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>
#include <iostream>
#include <string>
#include <iomanip>
#include <cstring>
#include <cstdlib>
#include <math.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <rosgraph_msgs/Clock.h>
#include <tf/transform_listener.h>

#include "rrg_nf_kurier_head/fheaddrives.h"
#include "kurier_face_tracker/pointtofollow.h"

#include "rrg_navigation_dynamic_msgs/people.h"
#include "rrg_navigation_dynamic_msgs/person.h"

struct Point
{
    int x;
    int y;
};

class emotions
{
public:
    emotions(ros::NodeHandle n);
    ~emotions();

    rrg_nf_kurier_head::fheaddrives check_range(rrg_nf_kurier_head::fheaddrives fdrives1);
    void users_faces();
    void choice();
    void happiness();
    void anger();    
    void move_head();   
    rrg_nf_kurier_head::fheaddrives servos_zero(rrg_nf_kurier_head::fheaddrives fdrives1);
    rrg_nf_kurier_head::fheaddrives motors_zero(rrg_nf_kurier_head::fheaddrives fdrives1);
    void head_zero();
    void yes_nod();
    void no_turn();
    void left_turn();
    void right_turn();
    int demo(int b);  
    void callbackPointToFollow(const kurier_face_tracker::pointtofollow &facecenter);
    void face_follow_move(const kurier_face_tracker::pointtofollow &facecenter);
    void head_follow_move(const kurier_face_tracker::pointtofollow &facecenter, int framewidth, int frameheight, int avgturn, int avgnod);
    void callbackPeople(const rrg_navigation_dynamic_msgs::people &people);
    void Camera_Follower();
    void Advanced_Follower();
    
    rrg_navigation_dynamic_msgs::person checkDistance(std::vector<rrg_navigation_dynamic_msgs::person> peoplelist);
    void VectorUpdate(const rrg_navigation_dynamic_msgs::people &people);
    void fromKinectToHead(rrg_navigation_dynamic_msgs::person tempperson);
    
    
protected:
    
    int i, xd, yd, s0, s1, temp0, temp1, counter, templistsize, closestID, s2;
    
    Point sum, fcenter, ccenter, difference, average;
    
    double temp2, temp3, KKinect, kinectvalue;
    
    bool flag; // cam is working 
    bool flag1, flag2; // kinect is working
    
    std::vector<Point> list;
    std::vector<rrg_navigation_dynamic_msgs::person> peoplelist;
    
    emotions *expression;
    
    ros::NodeHandle n;
        
    ros::Publisher pubfheaddrives;
    ros::Subscriber subPointToFollow;
    ros::Subscriber subPeople;
    
    rrg_nf_kurier_head::fheaddrives fdrives1, fdrives2;
    kurier_face_tracker::pointtofollow facecenter, facecenter_global;
    rrg_navigation_dynamic_msgs::people people, people_global;
    rrg_navigation_dynamic_msgs::person tempperson, tempperson1;
};


#endif	/* EMOTIONS_H */
