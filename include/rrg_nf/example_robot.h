/* 
 * File:   example_robot.h
 * Author: mateusz
 *
 * Created on December 6, 2012, 7:39 PM
 */


#ifndef EXAMPLE_ROBOT_H
#define	EXAMPLE_ROBOT_H


#define DEFAULT_NFV2_BAUD B57600
#define COMM_BUFSZ 256
#define COMMAND_ARRAY_SZ 16


#define robot_left_max 20
#define robot_right_max 20

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <iostream>
#include <iomanip>
#include <cstring>
#include "rrg_nf/nfv2.h"
#include "rrg_nf/serial_posix.h"

#include "rrg_nf_kurier_head/fheaddrives.h"

#include <time.h>
#include <math.h>

class Robot
{
public:

    Robot()
    {
    }

    ~Robot()
    {
    }

    int robot_speed_left;
    int robot_speed_right;


    double robot_speed_x;
    double robot_speed_y;
    double robotspeed;
    double robotspeedangular;
    double posx;
    double posy;
    double orient; //in radians

};

class example_robot
{
public:
    example_robot(ros::NodeHandle n, tf::TransformListener& tf);
    ~example_robot();

    void sendToRobot();
    void readFromRobot();

    void robotUpdate();
    void robotInit();
    void robotPanic();
    void robotGetVitals();
    void robotSetDO(int8_t SetDO);
    void robotSetAO();
    void robotGetDI();
    void robotGetAI();
    void setRobotServos(const rrg_nf_kurier_head::fheaddrives &msg2);
    void setMotorsSpeeds(int motor_left, int motor_right);
    void setMotorsPositions(const rrg_nf_kurier_head::fheaddrives &msg2);

    void callbackCmdVel(const geometry_msgs::TwistConstPtr &msg);
    void callbackFHeadDrives(const rrg_nf_kurier_head::fheaddrives &msg2);

    double stupidcounter;
    
private:

    ros::NodeHandle n;
    tf::TransformListener& _tf;

    NF_STRUCT_ComBuf NFComBuf;
    uint8_t commandArray[COMMAND_ARRAY_SZ];
    uint8_t rxCnt, txCnt, commandCnt;

    SerialComm *CommPort;
    std::string portName;
    uint8_t rxBuf[COMM_BUFSZ];
    uint8_t txBuf[COMM_BUFSZ];

    ros::Subscriber subCmdVel;
    ros::Subscriber subFHeadDrives;
    
    rrg_nf_kurier_head::fheaddrives msg2;

    Robot *robot;

};



#endif	/* EXAMPLE_ROBOT_H */

